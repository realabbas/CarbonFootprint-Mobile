## [CarbonFootprint-Mobile](https://gitlab.com/aossie/CarbonFootprint-Mobile)

### [Ritesh Agrawal](https://gitlab.com/ritesh-ag)

#### Working APK:- https://bit.ly/2DanqQO

#### Proposed Goal
My proposed goal for the project:- https://summerofcode.withgoogle.com/projects/#6647813162663936

Link to the proposal:- https://docs.google.com/document/d/1OfiUi-WJaLcJAeX6X3GNgI5wajBiYI4O_cVxLvhR1ew/edit?usp=sharing

#### Journey

Major goal of my GSoC period was to update the project to latest version of React Native along with the dependencies, worked on the performance of the application and shifted to MapBox from Google Map. For better performance, we shifted to React hooks, fixed few memory leakages and reduced app size by removing reduntant files and modules. Reporting crash reports was also important, so we implemented Firebase Crashlytics. For better authentication of user, we added email verification feature. Added Privacy policy and Terms and conditions. Lastly fixed few bugs and UI related issues.

#### Detailed Technological Explanation

*_Phase 1_* :- 
* Updated React Native version to 0.61.5 and few library modules.
* Added scripts and error and fixes for mac.
* Updated all class components to functional components using React Hooks.
* Refactored codebase and removed reduntant files and lines of code.
* Fixed memory leak in some components.

*_Phase 2_* :-
* Shifted from Google Map to MapBox.
* Added place search component using MapBox API.
* Removed unused library modules and updated few modules.

*_Phase 3_* :-
* Implemented email verification feature.
* Implemented Firebase Crashlytics for storing crash reports in firebase.
* Added Privacy policy and Terms and conditions in the application.
* Fixed few bugs and UI related issues.

#### MRs
* [MR 215 - Updated React Native version to 0.61.5 and few library modules](https://gitlab.com/aossie/CarbonFootprint-Mobile/-/merge_requests/215) *_MERGED_*

* [MR 222 - Updated all class components to functional using React Hooks, Refactored the codebase and removed reduntant lines of code and Fixed memory leak ](https://gitlab.com/aossie/CarbonFootprint-Mobile/-/merge_requests/222) *_MERGED_*

* [MR 224 - Added scripts for mac](https://gitlab.com/aossie/CarbonFootprint-Mobile/-/merge_requests/224) *_MERGED_*

* [MR 225 - Shifted from Google Map to MapBox and implemented place search component using mapbox api](https://gitlab.com/aossie/CarbonFootprint-Mobile/-/merge_requests/225) *_MERGED_*

* [MR 226 - Removed unused modules and updated few modules](https://gitlab.com/aossie/CarbonFootprint-Mobile/-/merge_requests/226) *_MERGED_*

* [MR 227 - Email verification feature](https://gitlab.com/aossie/CarbonFootprint-Mobile/-/merge_requests/227) *_MERGED_*

* [MR 228 - Firebase Crashlytics Implementation](https://gitlab.com/aossie/CarbonFootprint-Mobile/-/merge_requests/228) *_MERGED_*

* [MR 229 - Added Privacy Policy and Terms and conditions](https://gitlab.com/aossie/CarbonFootprint-Mobile/-/merge_requests/229) *_MERGED_*

